document.getElementById("hello_text").textContent = "はじめてのjavascript";

// キーが押された時の処理
document.addEventListener("keydown", onKeyDown);
function onKeyDown(event) {
  if(event.keyCode == 37) {
    moveLeft();
  } else if(event.keyCode == 39) {
    moveRight();
  } else if(event.keyCode == 40) {
    fallBlocks();
  }
}

var count = 0; // 秒数カウント用
var cells; // 表のセル用
var isStart = true;

// ブロックのパターン
var blocks = {
  i: {
    class: "i",
    pattern: [
      [1, 1, 1, 1]
    ]
  },
  o: {
    class: "o",
    pattern: [
      [1, 1],
      [1, 1]
    ]
  },
  t: {
    class: "t",
    pattern: [
      [0, 1, 0],
      [1, 1, 1]
    ]
  },
  s: {
    class: "s",
    pattern: [
      [0, 1, 1],
      [1, 1, 0]
    ]
  },
  z: {
    class: "z",
    pattern: [
      [1, 1, 0],
      [0, 1, 1]
    ]
  },
  j: {
    class: "j",
    pattern: [
      [1, 0, 0],
      [1, 1, 1]
    ]
  },
  l: {
    class: "l",
    pattern: [
      [0, 0, 1],
      [1, 1, 1]
    ]
  }
};

var ends = { // ENDの文字を浮かび上がらせるようのパターン
  z: {
    class: "z",
    pattern: [
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 1, 1, 1, 1, 1, 1, 0, 0],
      [0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
      [0, 0, 1, 1, 1, 1, 1, 1, 0, 0],
      [0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
      [0, 0, 1, 1, 1, 1, 1, 1, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ]
  },
  j: {
    class: "j",
    pattern: [
      [0, 0, 1, 1, 0, 0, 0, 1, 0, 0],
      [0, 0, 1, 1, 1, 0, 0, 1, 0, 0],
      [0, 0, 1, 1, 0, 1, 0, 1, 0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1, 0, 0],
      [0, 0, 1, 1, 0, 0, 0, 1, 0, 0]
    ]
  },
  l: {
    class: "l",
    pattern: [
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 1, 1, 1, 1, 1, 0, 0, 0],
      [0, 0, 1, 1, 0, 0, 0, 1, 0, 0],
      [0, 0, 1, 1, 0, 0, 0, 1, 0, 0],
      [0, 0, 1, 1, 0, 0, 0, 1, 0, 0],
      [0, 0, 1, 1, 1, 1, 1, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ]
  }
};

loadTable(); // 表を読み込む
//-------------------- 1秒ごとに処理 --------------------
var id = setInterval(function(){ playGame() }, 1000);

//-------------------- ゲームをプレイ中 --------------------
function playGame() {
  count++;
  document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")";

  if(hasFallingBlock()) {
    fallBlocks();
    checkOver();
  } else {
    deleteRow();
    generateBlock();
  }

  if(!isStart){
    clearInterval(id);
    endGame();
  }
}

//-------------------- ゲーム終了(ENDの文字を浮かばせたかった) --------------------
function endGame() {
  var keys = Object.keys(ends);

  var endBlock;
  var endBlockKey;
  var blockCount = 0;
  for(var i = 0; i < keys.length; i++) { // endsのkeyの数だけ繰り返し
    endBlockKey = keys[i];
    endBlock = ends[endBlockKey];
    var pattern = endBlock.pattern;
    for(var row = 0; row < pattern.length; row++) {
      for(var col = 0; col < pattern[row].length; col++) {
        if(pattern[row][col]) {
          cells[row + blockCount][col].className = endBlock.class;
          cells[row + blockCount][col].blockNum = i + 1;
        }
      }
    }
    blockCount += pattern.length;
  }
}

//-------------------- 表の読み込み --------------------
function loadTable() {
  cells = [];
  var td_array = document.getElementsByTagName("td");
  var index = 0;
  for(var row = 0; row < 20; row++) {
    cells[row] = [];
    for(var col = 0; col < 10; col++) {
      cells[row][col] = td_array[index];
      index++;
    }
  }
}

//-------------------- ブロック落下 --------------------
function fallBlocks() {
  // 1. 底についてないか
  for(var i = 0; i < 10; i++) {
    if(cells[19][i].blockNum === fallingBlockNum) {
      isFalling = false;
      return;
    }
  }

  // 2. 1マス下に別のブロックはないか
  for(var row = 18; row >= 0; row--) {
    for(var col = 0; col < 10; col++) {
      if(cells[row][col].blockNum === fallingBlockNum) {
        if(cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum) {
          isFalling = false;
          return;
        }
      }
    }
  }

  // 下から二番目の行から繰り返しクラスを下げていく
  for(var row = 18; row >= 0; row--) {
    for(var col = 0; col < 10; col++) {
      if(cells[row][col].blockNum === fallingBlockNum) {
        cells[row + 1][col].className = cells[row][col].className;
        cells[row + 1][col].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}

//-------------------- 落下しているか判定 --------------------
var isFalling = false;
function hasFallingBlock() { // 落下中のブロックがあるか確認
  return isFalling;
}

//-------------------- 揃っている行を消す --------------------
function deleteRow() {
  for(var row = 19; row >= 0; row--) {
    var canDelete = true;
    for(var col = 0; col < 10; col++) {
      if(cells[row][col].className === "") {
        canDelete = false;
      }
    }
    if(canDelete) {
      // 1行消す
      for(var col = 0; col < 10; col++) {
        cells[row][col].className = "";
      }
      // 上の行のブロックを全て1マス落とす
      for(var downRow = row - 1; downRow >= 0; downRow--) {
        for(var col = 0; col < 10; col++) {
          cells[downRow + 1][col].className = cells[downRow][col].className;
          cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
          cells[downRow][col].className = "";
          cells[downRow][col].blockNum = null;
        }
      }
    }
  }
}

//-------------------- 新規ブロック生成 --------------------
var fallingBlockNum = 0;
function generateBlock() { // ランダムにブロックを生成
  // ランダムにブロックを生成する
  // 1. ブロックパターンからランダムに1つパターンを選ぶ
  var keys = Object.keys(blocks);
  var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
  var nextBlock = blocks[nextBlockKey];
  var nextFallingBlockNum = fallingBlockNum + 1;

  // 2. 選んだパターンをもとにブロックを配置する
  var pattern = nextBlock.pattern;
  for(var row = 0; row < pattern.length; row++) {
    for(var col = 0; col < pattern[row].length; col++) {
      if(pattern[row][col]) {
        cells[row][col + 3].className = nextBlock.class;
        cells[row][col + 3].blockNum = nextFallingBlockNum;
      }
    }
  }

  // 3. 落下中のブロックがあるとする
  isFalling = true;
  fallingBlockNum = nextFallingBlockNum;
}

//-------------------- 終了判定 --------------------
function checkOver() {
  for(var row = 0; row < 2; row++) {
    for(var col = 0; col < 10; col++) {
      // 上二段にブロックが存在する && ブロックが落下していない
      if(cells[0][col].className !== "" && !hasFallingBlock()) {
        // Game Over
        alert("game over");
        init();
        isStart = confirm("もう一度遊びますか？");
      }
    }
  }
}

//-------------------- 初期化 --------------------
function init() {
  for(var row = 0; row < 20; row++) {
    for(var col = 0; col < 10; col++) {
      cells[row][col].className = "";
      cells[row][col].blockNum = null;
    }
  }
  count = 0;
}

//-------------------- 右に移動 --------------------
function moveRight() { // ブロックを右に移動
  var canRight = true;

  // 右側の壁に触れている？
  for(var row = 0; row < 20; row++) {
    if(cells[row][9].blockNum === fallingBlockNum) {
      canRight = false;
      return;
    }
  }

  // 右側にブロックがある？
  for(var row = 0; row < 20; row++) {
    for(var col = 8; col >= 0; col--) {
      if(cells[row][col].blockNum === fallingBlockNum) {
        if(cells[row][col + 1].className !== "" && cells[row][col + 1].blockNum !== fallingBlockNum) {
          canRight = false;
          return;
        }
      }
    }
  }

  for(var row = 0; row < 20; row++) {
    for(var col = 9; col >= 0; col--) {
      if(cells[row][col].blockNum === fallingBlockNum && canRight) {
        cells[row][col + 1].className = cells[row][col].className;
        cells[row][col + 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}

//-------------------- 左に移動 --------------------
function moveLeft() { // ブロックを左に移動
  var canLeft = true;

  // 左の壁に触れてる？
  for(var row = 0; row < 20; row++) {
    if(cells[row][0].blockNum === fallingBlockNum) {
      canLeft = false;
      return;
    }
  }

  // 左にブロックがある？
  for(var row = 0; row < 20; row++) {
    for(var col = 1; col < 10; col++) {
      if(cells[row][col].blockNum === fallingBlockNum) {
        if(cells[row][col - 1].className !== "" && cells[row][col - 1].blockNum !== fallingBlockNum) {
          canLeft = false;
          return;
        }
      }
    }
  }

  for(var row = 0; row < 20; row++) {
    for(var col = 0; col < 10; col++) {
      if(cells[row][col].blockNum === fallingBlockNum && canLeft) {
        cells[row][col - 1].className = cells[row][col].className;
        cells[row][col - 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}
